import express from "express";
import * as dotenv from "dotenv";
import cors from "cors";
import connectDB from "./mongodb/conect.js";
import postRoutes from "./routes/postRoutes.js";
import bootRoutes from "./routes/bootRoutes.js";

dotenv.config();

const app = express();
app.use(cors());
app.use(express.json({ limit: "50mb" }));

app.use("/api/v1/post", postRoutes);
app.use("/api/v1/boot", bootRoutes);

app.get("/", async (req, res) => {
  res.send("Hola BootCoop");
});

const startServer = async () => {
  try {
    connectDB(process.env.MONGO_URL);
    app.listen(8080, () =>
      console.log("Server has started on por http://localhost:8080")
    );
  } catch (error) {
    console.log(error);
  }
};

startServer();
