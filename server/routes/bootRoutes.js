import express from "express";
import * as dotenv from "dotenv";
import OpenAI from "openai";

dotenv.config();

const router = express.Router();

const openai = new OpenAI({
  apiKey: process.env.OPENIA_API_KEY,
  // baseURL: "https://api.openai.com/v1/",
  organization: "org-IwZmNxL0s86mnQ5Afp0oF90L",
});

console.log(openai);

router.route("/").get((req, res) => {
  console.log(openai);

  res.send("Hola from BootCooop API");
});

router.route("/").post(async (req, res) => {
  try {
    const { prompt } = req.body;
    const aiResponse = await openai.images.generate({
      prompt: prompt,
      n: 1,
      size: "1024x1024",
      response_format: "b64_json",
    });
    const image = aiResponse.data[0].b64_json;

    res.status(200).json({ photo: image });
  } catch (error) {
    console.log(error);
    res.status(500).send(error?.error.message);
  }
});

// (response_format = "b64_json")

export default router;
