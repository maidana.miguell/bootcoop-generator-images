import mongoose from "mongoose";

const connectDB = (url) => {
  mongoose.set("strictQuery", true);

  mongoose
    .connect(url)
    .then(() => console.log("Mondo DB connected"))
    .catch((err) => {
      console.log("Error al conectar a Mongo");
      console.log(err);
    });
};

export default connectDB;
